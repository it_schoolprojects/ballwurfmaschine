public class Test {

    @org.junit.Test
    public void test() {
        System.out.println(Functions.calcAngleIdealized(
                20, 0, 1.5, 13.5, false
        ));

        System.out.println(Functions.calcHeightFriction(
                10, 45, 1.5, 15, 2.5, 0.2, 0.45, 1.2041
        ));

        System.out.println("\nCalculated angle: " +
                Algorithms.calcAngleFrictionAlgorithm(
                        30, 5, 0, 30, 2.5, 0.2, 0.45, 1.2041, 10
                )
        );
    }
}
