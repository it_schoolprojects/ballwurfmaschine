public final class Algorithms {

    //PUBLIC

    public static double calcAngleFrictionAlgorithm(final double x, final double y, final double y_0, final double v_0, final double m, final double profile, final double c_w, final double density, final int precision) {

        double phi_approx = calcAngleFrictionNewton(x, y, y_0, v_0, m, profile, c_w, density, precision, false);

        if (Double.isInfinite(phi_approx))
            return 90; //TODO Prüfen ob die Höhe erreicht werden kann

        if (Double.isNaN(phi_approx))
            throw new UnsupportedOperationException("Calculation failed. Point cannot be reached.");

        if (phi_approx > 90 || phi_approx < 0) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.err.println("\nCalculated angle cannot be used. Recalculating angle ...\n");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return calcAngleFrictionNewton(x, y, y_0, v_0, m, profile, c_w, density, precision, true);
        }


        return phi_approx;
    }

    private static double calcAngleFrictionNewton(final double x, final double y, final double y_0, final double v_0, final double m, final double profile, final double c_w, final double density, final int precision, final boolean alternative) {
        double phi_approx = Functions.calcAngleIdealized(x, y, y_0, v_0, alternative); //Startwert ist der Winkel unter idealisierten Bedingungen
        double y_approx;
        double phi_approx_prev; //Loop-Prevention
        System.out.println("Start: " + phi_approx);
        while (Math.abs(Functions.calcHeightFriction(x, phi_approx, y_0, v_0, m, profile, c_w, density) - y) * Math.pow(10, precision) >= 1) { //Solange die gewünschte Präzision nicht erreicht ist, mache weiter
            phi_approx_prev = phi_approx;
            //Newton-Algorithm
            y_approx = Functions.calcHeightFriction(x, phi_approx, y_0, v_0, m, profile, c_w, density);
            phi_approx -= (y_approx - y) / (Functions.calcHeightFrictionDerivedPhi(x, phi_approx, v_0, m, profile, c_w, density)); // phi_n+1 = phi_n - y(phi_n)/y'(phi_n)
            System.out.println(phi_approx + " > " + Functions.calcHeightFriction(x, phi_approx, y_0, v_0, m, profile, c_w, density));
            if (phi_approx_prev == phi_approx)
                throw new UnsupportedOperationException("Calculation failed. Point cannot be reached.");
        }

        return phi_approx;
    }

    //PRIVATE

    private Algorithms() {
    }

}
