import java.util.Scanner;

public class Main {

    public static void main(String... args) {
        System.out.println("\nMass: 57.5g, Profile: 141cm^2, Density: 1.2041, c_w: 0.45\n");

        while (true) {
            Scanner sc = new Scanner(System.in);
            System.out.print("\nDistance: ");
            final double x = sc.nextDouble();
            System.out.print("\nStart-Height: ");
            final double y_0 = sc.nextDouble();
            System.out.print("\nEnd-Height: ");
            final double y = sc.nextDouble();
            System.out.print("\nVelocity: ");
            final double v_0 = sc.nextDouble();
            try {
                final double angle = Algorithms.calcAngleFrictionAlgorithm(x, y, y_0, v_0, 0.0575, 0.014, 0.45, 1.2041, 10);
                System.out.println("\nCalculated Angle: " + angle);
            } catch (UnsupportedOperationException e) {
                System.out.print("\nCalculated Angle: ");
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                System.err.println(e.getMessage());
            }
        }
    }

}
